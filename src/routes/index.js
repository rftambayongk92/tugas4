import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Splash, WelcomeAuth} from '../pages';

const Stack = createStackNavigator();

const Route = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name="Splash" component={Splash}/> 
            
            <Stack.Screen name="WelcomeAuth" component={WelcomeAuth}/>  
        </Stack.Navigator>
    )
}
export default Route