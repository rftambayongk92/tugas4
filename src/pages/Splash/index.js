import React, {useEffect} from 'react';
import { View, Text,Image} from 'react-native';


const Splash = ({navigation}) => {
    useEffect(()=> {
        setTimeout(() => {
             navigation.navigate('WelcomeAuth')
        }, 5000)
    },)
    return (
        <View>
            <Image source={require('../Splash/org.jpg')}style={{width:360,height:590,}}></Image>
            <Text style={{alignItems:'center',justifyContent:'center',backgroundColor:'green',marginTop:0,borderRadius:5,
        textAlign:'center'}}>selamat datang bersiaplah dalam lima detik anda akan berpindah kehalaman berikutnya. simsalabim 12345</Text>
        </View>
    );
};

export default Splash;